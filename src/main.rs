use std::io;

fn main() {
}
fn duplicate()
{
    println!("This program removes duplicates in an array.");
    println!("Please enter size of array: ");
    let mut n = String::new();
    io::stdin().read_line(&mut n).unwrap();

    let n: usize = n.trim().parse().unwrap();
    let mut line = String::new();

    println!("Please enter the values of array: ");
    println!("Example: 1 2 2 4");
    io::stdin().read_line(&mut line).unwrap();
    let mut sequence: Vec<u64> = Vec::with_capacity(n);

    for i in line.split_whitespace() {
        sequence.push(i.parse().unwrap());
    }

    let mut i = n - 1;
    loop {
        let test = sequence[i];
        for j in (0..i).rev() {
            if sequence[j] == test {
                sequence.remove(j);
                i -= 1;
            }
        }

        if i > 0 {
            i -= 1;
        } else {
            break;
        }
    }

    println!("Size of array: {}", sequence.len());
    for i in sequence {
        print!("{} ", i);
    }
}

fn elevator_stairs() {
    println!("Should you take the elevator or stairs?");
    println!("Please enter values respectively:
        1. the floor you are at
        2. floor you want to go
        3. the floor where the elevator is located on
        4. time it takes you pass between to floors by stairs
        5. time it takes the elevator to pass between two floors
        6. time it takes for the elevator to close or open the doors
        (ex. 5 1 4 4 2 1)");

    let mut s = String::new();
    io::stdin().read_line(&mut s).unwrap();
    let mut s = s.split_whitespace();

    let x: i32 = s.next().unwrap().parse().unwrap();
    let y: i32 = s.next().unwrap().parse().unwrap();
    let z: i32 = s.next().unwrap().parse().unwrap();
    let t1: i32 = s.next().unwrap().parse().unwrap();
    let t2: i32 = s.next().unwrap().parse().unwrap();
    let t3: i32 = s.next().unwrap().parse().unwrap();

    if (x - y).abs() * t1 < ((x - z).abs() + (x - y).abs()) * t2 + 3 * t3 {
        println!("It's faster to take the stairs!");
    } else {
        println!("It's faster to take the elevator!");
    }

fn subtraction() {
     println!("The number from which you will subtract and the number of subtractions correspondingly");
     println!("Example: 512 4");
     let mut input = String::new();
     std::io::stdin().read_line(&mut input).expect("Wrong input!");

     let mut input_numbers: Vec<i32> = input.trim().splitwhitespace().map(|s| s.parse::<i32>().unwrap()).collect();
        for _ in 0..input_numbers[1] {
            if input_numbers[0] % 10 == 0 {
                input_numbers[0] /= 10;
                continue;
            }
            input_numbers[0] -= 1;
        }
        println!("Answer: {}", input_numbers[0]);
    }
}